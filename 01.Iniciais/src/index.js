import React from 'react'
import ReactDOM from 'react-dom'
//import PrimeiroComponent from './Components/PrimeiroComponent'
// import {CompA, CompB as B} from './Components/DoisComponents'
//import CompA, { CompB as B} from './Components/DoisComponents'
// import MultiElementos from './Components/MultiElementos'
// import FamiliaSilva from './Components/FamiliaSilva'
// import Familia from './Components/Familia'
// import Membro from './Components/Membro'
// import ComponenteComFuncao from './Components/ComponenteComFuncao'
// import Pai from './Components/Pai'
// import ComponentClasse from './Components/ComponenteClasse'
// import Contador from './Components/Contador'
import Hook from './Components/Hook'

const elemento = document.getElementById('root')

ReactDOM.render(
    <div>
        <Hook />
        {/* <Contador numeroInicial={100}/> */}
        {/* <ComponentClasse valor="Sou um Componente de Classe!"/> */}
        {/* <Pai /> */}
        {/* <ComponenteComFuncao />
        <ComponenteComFuncao /> */}
        {/* <Familia sobrenome="Pereira">
            <Membro nome="Andre"  />
            <Membro nome="Mariana"  />
        </Familia>
        <Familia sobrenome="Arruda">
            <Membro nome="Bia"  />
            <Membro nome="Gustavo"  />
        </Familia> */}
        {/* <FamiliaSilva /> */}
        {/* <MultiElementos /> */}
        {/* <CompA valor="Olá, eu sou A!"/>
        <B valor="B na área!"/> */}
        {/* <PrimeiroComponent valor="Bom dia"/> */ }
    </div>    
, elemento)

//const jsx = <h1>Olá, React</h1>
//ReactDOM.render(jsx, elemento)
