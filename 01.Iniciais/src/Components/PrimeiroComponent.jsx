import React from 'react'

export default (props) => 
   <div>
        <h1>{props.valor}</h1>
   </div>

// export default () => 
//    <div>
//         <h1>Pimeiro Componente (Arrow)!</h1>
//    </div>

// export default function(){
//     return <h1>Pimeiro Componente!</h1>
// }

// function primeiro() {
//     return <h1>Pimeiro Componente!</h1>
// }

// export default primeiro