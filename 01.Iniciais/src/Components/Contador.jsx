import React, { Component} from 'react'

export default class Contador extends Component {
    // constructor(props){
    //     super(props)
    //     this.maisUm = this.maisUm.bind(this)
    // }
    
    // maisUm() {
    //     this.props.numero++
    // }

    state = {
        numero: this.props.numeroInicial
    }
    maisUm = () => {
        this.alterarNumero(1)
        //this.setState({ numero: this.state.numero + 1 })
        //this.state.numero++
    }

    menosUm = () => {
        this.alterarNumero(-1)
        //this.setState({ numero: this.state.numero - 1 })
    }

    alterarNumero = diferenca => {
        this.setState({ 
            numero: this.state.numero +diferenca 
        })
    }
    render() {
        return (
            <div>
                Número: {this.state.numero}
                {/* <button onClick={this.maisUm}>Inc</button> */}
                {/* <button onClick={() => this.maisUm()}>Inc</button> */}
                <button onClick={this.maisUm}>Inc</button>
                <button onClick={this.menosUm}>Dec</button>
                <button 
                    onClick={ () => this.alterarNumero(10)}>
                    Inc 10
                </button>
                <button 
                    onClick={ () => this.alterarNumero(-10)}>
                    Dec 10
                </button>
            </div>
        )
    }
}