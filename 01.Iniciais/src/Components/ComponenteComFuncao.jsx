import React from 'react'

const aprovados = ['Paula', 'Roberta', 'Gustavo', 'Julia']
    
export default props => {
    const gerarItem = items => {
        return items.map(item => <li>{item}</li>)
    }

    return (
        <ul>
            {gerarItem(aprovados)}
        </ul>
    )
}