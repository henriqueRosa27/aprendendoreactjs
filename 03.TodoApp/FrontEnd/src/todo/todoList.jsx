import React from 'react';
import IconButton from '../template/iconButton';

export default props => {

    const renderRows = () => {
        const list = props.list || [];
        return list.map((tarefa) => (
            <tr key={tarefa._id}>
                <td className={tarefa.done ? 'markedAsDone' : ''}>
                    {tarefa.description}
                </td>
                <td>
                    <IconButton hide={tarefa.done}
                        style='success' 
                        icon='check' 
                        onClick={() => props.handleMarkAsDone(tarefa)}/>
                    <IconButton  hide={!tarefa.done}
                        style='warning' 
                        icon='undo' 
                        onClick={() => props.handleMarkAsPending(tarefa)}/>
                    <IconButton hide={!tarefa.done}
                        style='danger' 
                        icon='trash-o' 
                        onClick={() => props.handleRemove(tarefa)}/>
                </td>
            </tr>
        ))
    }

    return (
        <table className='table'>
            <thead>
                <tr>
                    <th>
                        Descrição
                    </th>
                    <th className='tableActions'>
                        Ações
                    </th>
                </tr>
            </thead>
            <tbody>
                {renderRows()}
            </tbody>
        </table>
    )
}