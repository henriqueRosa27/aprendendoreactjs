import React from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';

import IconButton from '../template/iconButton';
import { markAsDone, markAsPending, remove } from './todoActions';

const TodoList = props => {

    const renderRows = () => {
        const list = props.list || [];
        return list.map((tarefa) => (
            <tr key={tarefa._id}>
                <td className={tarefa.done ? 'markedAsDone' : ''}>
                    {tarefa.description}
                </td>
                <td>
                    <IconButton hide={tarefa.done}
                        style='success' 
                        icon='check' 
                        onClick={() => props.markAsDone(tarefa)}/>
                    <IconButton  hide={!tarefa.done}
                        style='warning' 
                        icon='undo' 
                        onClick={() => props.markAsPending(tarefa)}/>
                    <IconButton hide={!tarefa.done}
                        style='danger' 
                        icon='trash-o' 
                        onClick={() => props.remove(tarefa)}/>
                </td>
            </tr>
        ))
    }

    return (
        <table className='table'>
            <thead>
                <tr>
                    <th>
                        Descrição
                    </th>
                    <th className='tableActions'>
                        Ações
                    </th>
                </tr>
            </thead>
            <tbody>
                {renderRows()}
            </tbody>
        </table>
    )
}

const mapStateToProps = state => ({list: state.todo.list});
const mapDispatchToProps = dispatch => bindActionCreators({ markAsDone, markAsPending, remove }, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(TodoList);