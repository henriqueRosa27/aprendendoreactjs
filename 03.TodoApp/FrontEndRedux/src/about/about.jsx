import React from 'react';
import PageHeader from '../template/pageHeader';

export default props => (
    <div>
        <PageHeader name="Sobre" small="Nós" />

        <h2>Nossa História</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eget ante ut turpis tempus elementum. Curabitur ultricies, mauris non consequat ullamcorper, eros lorem ullamcorper est, et mollis odio eros at ligula. Pellentesque placerat imperdiet risus dapibus tempor. Donec sed metus turpis. Donec eget justo non libero iaculis tempus at faucibus lacus. Donec laoreet nunc id neque euismod consectetur. Pellentesque nunc urna, gravida vel posuere eu, ullamcorper vitae risus. Aliquam lorem nibh, sodales id nibh sed, vulputate fermentum mi. Suspendisse convallis venenatis nibh. Sed congue semper justo, in malesuada ipsum viverra finibus. Morbi ultrices suscipit libero aliquam facilisis. Praesent facilisis non ligula ac scelerisque. Sed faucibus ac nulla ac sodales.</p>

        <h2>Missão e Visão</h2>
        <p>Etiam euismod sagittis massa quis convallis. Nam lorem lacus, ornare quis suscipit non, maximus eget purus. Donec tellus mi, fermentum vitae ante sed, vestibulum vulputate orci. Integer commodo tellus vel faucibus ultrices. Nunc facilisis eleifend metus, at auctor ligula lobortis cursus. Sed iaculis purus eu orci lacinia elementum. Nulla non tellus ac ex consectetur cursus. Maecenas viverra tellus lobortis nunc viverra, nec pretium tellus feugiat. Praesent sed enim id purus congue tristique. Curabitur tincidunt nulla sed lorem luctus, id laoreet est gravida. Maecenas sit amet congue nisl. Duis feugiat tincidunt sem, sed cursus mauris pretium id. Pellentesque sed lorem pretium, ultrices felis eu, condimentum odio. Aenean quis tellus ac arcu mollis lacinia. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum aliquet tempor purus non elementum.</p>

        <h2>Imprensa</h2>
        <p>Nulla posuere lacus et imperdiet luctus. Nulla interdum ante ut risus gravida dignissim. Morbi nec sapien magna. Nunc justo velit, tincidunt a nunc a, sodales cursus arcu. Morbi pretium, nulla quis volutpat lobortis, urna magna fringilla arcu, vitae accumsan nunc libero ut neque. Nulla consectetur nunc et augue dignissim, a blandit arcu ultrices. Vivamus venenatis arcu sit amet ligula dignissim aliquam. Quisque a odio iaculis, interdum urna egestas, rutrum ligula. Proin eu faucibus enim. Praesent quam justo, venenatis euismod condimentum id, tincidunt sit amet velit.</p>
    </div>
)